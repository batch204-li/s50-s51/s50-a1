Creating a react application

	Syntax:
		npx create-react-app <project-name>

Delete unnecessary files from the newly created project

Application > src
- App.test.js
- index.css
- logo.svg
- reportWebVitals.js

Remove the importation of the deleted files:

Application > src > index.js
- index.css
- reportWebVitals.js (including reportWebVitals())

Application > src > App.js
- logo.svg
- elements under <div> tag