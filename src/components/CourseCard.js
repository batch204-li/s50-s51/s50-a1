import {Row, Col, Card} from 'react-bootstrap';

export default function CourseCard(){
    return(
        <Row className="my-3">
            <Col>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h4>Sample Course</h4>
                        </Card.Title>
                        <Card.Text>
                            <p className="my-0">Description</p>
                            <p className="mt-0">This is a sample course offering.</p>
                            <p className="my-0">Price:</p>
                            <p className="mt-0">PhP 40,000</p>
                            <button className="bg-primary rounded py-1 px-2 text-light border-0">Enroll</button>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}