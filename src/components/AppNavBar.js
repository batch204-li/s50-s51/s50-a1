// import Container from 'react-bootstrap/Container';
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';

import {Container, Nav, Navbar, NavDropdown} from 'react-bootstrap'

export default function AppNavBar(){
	return(
		<Navbar bg="light" expand="lg">
	      <Container>
	        <Navbar.Brand href="#home">Zuitt</Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
		    {/*ms->ml = margin start->margin left*/}
			{/*me->mr = margin end->margin right*/}
	          <Nav className="ms-auto">
	            <Nav.Link href="#home">Home</Nav.Link>
	            <Nav.Link href="#courses">Courses</Nav.Link>
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>
	)
}