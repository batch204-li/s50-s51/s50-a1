import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


// const name = 'John Smith';

// let element = <h1>Hello, {name}</h1>

// const user = {
//   firstName: 'Jane',
//   lastName: 'Smith'
// }


// function formatName(profile){
//   return profile.firstName + profile.lastName
// }

// element = <h1>Hello, {formatName(user)}</h1>

// root.render(element)